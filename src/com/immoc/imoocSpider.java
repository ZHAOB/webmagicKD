package com.immoc;

import com.test.TPPipeline;
import com.test.TPofModel;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.ConsolePageModelPipeline;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;

@HelpUrl("http://ifeve\\.com/page/\\d+")
@TargetUrl("http://www\\.imooc\\.com/view/\\d+")
public class imoocSpider {
	@ExtractBy("//div[@class='hd']/h2/text()")
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public static void main(String[] args) {
		OOSpider.create(Site.me().setSleepTime(1000),
				new TPPipeline(), TPofModel.class)
				.addUrl("http://www.imooc.com/course/list").thread(5).run();
	}

}
