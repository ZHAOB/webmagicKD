package com.test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class testSpider implements PageProcessor {

	String[] userAgentInfos = {
			"Opera/9.27 (Windows NT 5.2; U; zh-cn)",
			" Opera/8.0 (Macintosh; PPC Mac OS X; U; en)",
			"Mozilla/5.0 (Macintosh; PPC Mac OS X; U; en) Opera 8.0 ",
			"Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Version/3.1 Safari/52",
			"Mozilla/5.0 (iPhone; U; CPU like Mac OS X) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/4A",
			"Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13 ",
			"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.12) Gecko/20080219 Firefox/2.0.0.12 Navigator/9.0.0.6",
			"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; 360SE)",
			"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; 360SE)" };
	// 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
	private Site site = Site
			.me()
			.setRetryTimes(3)
			.setSleepTime(1000)
			.setUserAgent(
					userAgentInfos[(int) (1 + Math.random() * (10 - 1 + 1))]);

	public Site getSite() {
		return site;
	}

	public void process(Page page) {
		List<String> urls = page.getHtml().links()
				.regex("(Http://www\\.cao2000\\.com/most-popular/\\d+/)").all();
		//videos/9454/90-mm3/
		page.addTargetRequests(page.getHtml().links()
				.regex("(http://www\\.cao2000\\.com/videos/\\d+/\\d+/)").all());
		page.addTargetRequests(urls);
		Pattern pattern = Pattern
				.compile("http://www\\.cao2000\\.com/\\w+/\\d+/[a-z0-9A-Z]*/\\d+/\\d+/\\d+\\.mp4");
		String str = page.getHtml().toString();
		Matcher m = pattern.matcher(str);
		if (m.find()) {
			System.out.println(m.group(0));
		}
	}

	public static void main(String[] args) {
		Spider.create(new testSpider())
				.addUrl("http://www.cao2000.com/most-popular/").thread(5).run();
	}

}
