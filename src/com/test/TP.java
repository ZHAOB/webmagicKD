package com.test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class TP implements PageProcessor {
	
	String[] userAgentInfos = {
			"Opera/9.27 (Windows NT 5.2; U; zh-cn)",
			"Opera/8.0 (Macintosh; PPC Mac OS X; U; en)",
			"Mozilla/5.0 (Macintosh; PPC Mac OS X; U; en) Opera 8.0 ",
			"Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Version/3.1 Safari/52",
			"Mozilla/5.0 (iPhone; U; CPU like Mac OS X) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/4A",
			"Mozilla/5.0 (Windows; U; Windows NT 5.2) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13 ",
			"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.12) Gecko/20080219 Firefox/2.0.0.12 Navigator/9.0.0.6",
			"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; 360SE)",
			"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; 360SE)" };
	// 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
	private Site site = Site
			.me()
			.setRetryTimes(30)
			.setSleepTime(10000)
			.setUserAgent(
					userAgentInfos[(int) (1 + Math.random() * (10 - 1 + 1))]);

	public Site getSite() {
		return site;
	}

	public void process(Page page) {
		// videos/9454/90-mm3/
		page.addTargetRequest(page
				.getHtml()
				.xpath("//div[@class='post_content']/p/a[@class='more-link']/@href")
				.toString());
		
		page.addTargetRequests(page.getHtml().links()
				.regex("(http://ifeve\\.com/page/\\d+)").all());
		
		System.out.println("--------------------------------------------------------");
		System.out.println(page.getHtml().xpath(
				"//div[@class='post']/h3[@class='title']/text()"));
	}

	public static void main(String[] args) {
		Spider.create(new TP()).addUrl("http://ifeve.com/").thread(100).run();
	}

}
