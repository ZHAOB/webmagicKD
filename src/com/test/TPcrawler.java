package com.test;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.model.OOSpider;

public class TPcrawler {
	
	private static TPPipeline tpPipline = new TPPipeline();

	public static void main(String[] args) {
		OOSpider.create(
				Site.me()
						.setUserAgent(
								"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36"),
				tpPipline, TPofModel.class)
				.addUrl("Http://www.cao2000.com/most-popular/8/")
				.thread(100).run();

	}
}
