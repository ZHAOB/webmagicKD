package com.test;


import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;

@HelpUrl("Http://www.cao2000.com/most-popular/\\d+/")
@TargetUrl("http://www.cao2000.com/videos/\\d+/\\d+/")
public class TPofModel {

	@ExtractBy(value = "http://www\\.cao2000\\.com/\\w+/\\d+/[a-z0-9A-Z]*/\\d+/\\d+/\\d+\\.mp4",type = ExtractBy.Type.Regex)
	private String title;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
}
